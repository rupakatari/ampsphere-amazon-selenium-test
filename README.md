#### Scenario:

·        Open www.amazon.co.uk

·        Search for “Java for Dummies by Barry A. Burd”

·        Get the number of books (options) displayed on the current page

·        Place order on the first book available

·        Validate that the basket has 1 item

·        Click on the basket and click on “Proceed to Checkout”
 

Instructions:

·        This should be implemented as a framework using POM (using separate class files)

·        Use cucumber to define the scenario in a feature file using Gherkin (Given-When-Then)

·        Data parameterization through feature file to input the String (e.g: Java for Dummies by Barry A. Burd)

·        The report should provide a screenshot of the page if the test case fails

·        In a notepad or word document please provide the steps to run the Test Script 