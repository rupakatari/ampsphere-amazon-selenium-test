package net.ruvetech.bdd;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/main/resources/features",
        glue = {"net/ruvetech/bdd"},
        format = {"html:target/reports"}
)
public class RunAllTests {
}
