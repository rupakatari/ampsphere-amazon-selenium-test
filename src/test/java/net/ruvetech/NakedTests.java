package net.ruvetech;

import net.ruvetech.pom.AddToBasketPage;
import net.ruvetech.pom.AmazonHomePage;
import net.ruvetech.pom.SearchItemPage;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class NakedTests {
    private static String BASE_URL = "https://www.amazon.co.uk";
    private static String DRIVER_NAME = "/chromedriver";

    private WebDriver webDriver;

    AmazonHomePage amazonHomePage;
    SearchItemPage searchItemPage;
    AddToBasketPage addToBasketPage;


    @BeforeClass
    public static void setUp() {

        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + DRIVER_NAME);

    }

    @Before
    public void beforeEveryTest() {
        webDriver = new ChromeDriver();
        amazonHomePage = new AmazonHomePage(webDriver);
        searchItemPage = new SearchItemPage(webDriver);
        addToBasketPage = new AddToBasketPage(webDriver);
    }

    @After
    public void afterEveryTest() throws InterruptedException {
        Thread.sleep(5000);
        webDriver.quit();
    }

    @Test
    public void amazonCheckoutTest() {

        String currentWindow = webDriver.getWindowHandle();
        webDriver.switchTo().window(currentWindow);
        webDriver.navigate().to(BASE_URL);
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();

        amazonHomePage.searchItem("Java for Dummies by Barry A. Burd");
        amazonHomePage.selectSearchButton();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //amazonHomePage.selectItemByNameV2("Java For Dummies (For Dummies (Computers))");
        //amazonHomePage.selectItemByName("Java For Dummies (For Dummies (Computers))");
        amazonHomePage.selectSearchListByIndex(0);
        searchItemPage.clickPaperBackOption();
        addToBasketPage.itemAddToBasket();
        addToBasketPage.proceedToCheckout();
    }


}
