package net.ruvetech.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


import java.util.List;

public class AmazonHomePage extends BasePage {

    @FindBy(how = How.ID, using = "twotabsearchtextbox")
    WebElement searchBarTextField;

    @FindBy(how = How.CLASS_NAME, using = "nav-input")
    WebElement searchIconField;

    @FindBy(how = How.CSS, using = ".a-size-medium.s-inline.s-access-title.a-text-normal")
    List<WebElement> searchList;

    public AmazonHomePage(WebDriver driver) {
        super(driver);
    }

    public void searchItem(String searchItemName) {
        searchBarTextField.sendKeys(searchItemName);
    }


    public void selectSearchButton() {
        searchIconField.click();
    }

    //Method one
    public void selectItemByName(String name) {
        for (WebElement webElement : searchList) {
            if (webElement.getText().equals(name)) {
                webElement.click();
                return;
            }
        }
        throw new NoSuchElementException("No element with name: " + name);
    }

    //Method two
    public void selectItemByNameV2(String name) {
        webDriver.findElement(By.linkText(name)).click();
    }

    //Method three
    public void selectSearchListByIndex(int index) {
        if (index < 0) {
            throw new IllegalArgumentException("Not a valid index " + index);
        }
        if (searchList != null && searchList.size() > 0) {
            searchList.get(index).click();
        } else {
            throw new RuntimeException("Search list is empty");
        }
    }
}
