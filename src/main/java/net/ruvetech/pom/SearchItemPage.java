package net.ruvetech.pom;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SearchItemPage extends BasePage {


    @FindBy(how = How.ID, using = "a-autoid-1-announce")
    WebElement paperBackElement;

    public SearchItemPage(WebDriver driver) {
        super(driver);
    }

    public void clickPaperBackOption() {
        paperBackElement.click();
    }
}
