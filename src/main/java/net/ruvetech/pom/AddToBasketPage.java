package net.ruvetech.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import sun.jvm.hotspot.runtime.Thread;

public class AddToBasketPage extends BasePage {

    @FindBy(how = How.ID, using = "add-to-cart-button")
    WebElement addToBasketButton;

    @FindBy(how = How.XPATH, using = "//*[@id='hlb-subcart']/div[1]/span")
    private WebElement basketCountElement1;

    @FindBy(how = How.ID, using = "hlb-ptc-btn-native")
    WebElement proceedToCheckoutButton;

    public AddToBasketPage(WebDriver driver) {
        super(driver);
    }

    public boolean isAddToBasketPageShown() {
        return addToBasketPageShown;
    }

    private boolean addToBasketPageShown = true;

    public void itemAddToBasket() {

        try {
            WebDriverWait wait = new WebDriverWait(webDriver, 10);
            WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.id("add-to-cart-button")));
            addToBasketButton.click();
        } catch (org.openqa.selenium.TimeoutException ex) {
            System.err.println("Element not present");
            addToBasketPageShown = false;
        }
    }

    public int getNumberOfItemsInBasket() {
        String combinedText = basketCountElement1.getText();
        if (combinedText.indexOf("(") > 0) {
            int index = combinedText.indexOf("(") + 1;
            String quantityStr = combinedText.substring(index, index + 1);
            return new Integer(quantityStr).intValue();
        } else {
            return -1;
        }
    }

    public void proceedToCheckout() {
        proceedToCheckoutButton.click();
    }
}
