package net.ruvetech.bdd;

import com.google.inject.Inject;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;
import net.ruvetech.pom.AddToBasketPage;
import net.ruvetech.pom.AmazonHomePage;
import net.ruvetech.pom.SearchItemPage;
import org.junit.Assert;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

@ScenarioScoped
public class UserCheckoutStepDefinition {

    private WebDriver webDriver;

    private AmazonHomePage amazonHomePage;
    private SearchItemPage searchItemPage;
    private AddToBasketPage addToBasketPage;

    //TODO - move to properties file
    private String BASE_URL = "https://www.amazon.co.uk";
    private String loginPageTitle = "Amazon Sign In";

    @Inject
    public UserCheckoutStepDefinition(ScenarioContainer scenarioContainer) {
        webDriver = scenarioContainer.webDriver;
        amazonHomePage = new AmazonHomePage(webDriver);
        searchItemPage = new SearchItemPage(webDriver);
        addToBasketPage = new AddToBasketPage(webDriver);
    }


    @Given("^User is on Amazon website$")
    public void user_is_on_Amazon_website() throws Throwable {
        String currentWindow = webDriver.getWindowHandle();
        webDriver.switchTo().window(currentWindow);
        webDriver.navigate().to(BASE_URL);
        try {
            webDriver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        } catch (TimeoutException e) {
            e.printStackTrace();
            //gulp
        }
        webDriver.manage().window().maximize();
    }

    @Given("^User enter search item text as \'([^\"]*)\'$")
    public void user_enter_search_item_text_as(String searchText) throws Throwable {
        amazonHomePage.searchItem(searchText);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Given("^User clicks search button$")
    public void user_clicks_search_button() throws Throwable {
        amazonHomePage.selectSearchButton();
    }

    @Given("^User clicks on the first item from the list displayed on the website$")
    public void user_clicks_on_the_first_item_from_the_list_displayed_on_the_website() throws Throwable {
        amazonHomePage.selectSearchListByIndex(0);
    }

    @Given("^User clicks on paperback option$")
    public void user_clicks_on_paperback_option() throws Throwable {
        searchItemPage.clickPaperBackOption();
    }

    @Given("^User clicks AddToBasket button$")
    public void user_clicks_AddToBasket_button() throws Throwable {
        addToBasketPage.itemAddToBasket();
    }

    @Given("^User could see (\\d+) item added to the basket$")
    public void User_could_see_1_item_added_to_the_basket(int numberOfItems) throws Throwable {
        if (addToBasketPage.isAddToBasketPageShown()) {
            int basketLength = addToBasketPage.getNumberOfItemsInBasket();
            Assert.assertEquals(numberOfItems, basketLength);
        }
    }

    @When("^User clicks ProceedToCheckout button$")
    public void user_clicks_ProceedToCheckout_button() throws Throwable {
        if (addToBasketPage.isAddToBasketPageShown()) {
            addToBasketPage.proceedToCheckout();
        }
    }

    @Then("^User is redirected to login page$")
    public void user_LoginPage_should_display() throws Throwable {
        if (addToBasketPage.isAddToBasketPageShown()) {
            Assert.assertEquals(loginPageTitle, webDriver.getTitle());
        } else {
            Assert.assertEquals("Amazon.co.uk Sign In", webDriver.getTitle());
        }
    }

}
