package net.ruvetech.bdd;

import com.google.inject.Inject;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CucumberApplicationHooks {

    static {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/chromedriver");
        System.out.println(System.getProperty("webdriver.chrome.driver"));
    }

    @Inject
    ScenarioContainer scenarioContainer;

    @Before
    public void beforeEveryMethod() {
        scenarioContainer.webDriver = new ChromeDriver();
    }

    @After
    public void afterEveryTest(Scenario scenario) throws InterruptedException {

        if (scenario.isFailed()) {
            //TODO - replace sout with log4j
            System.out.println(scenario.getName() + " failed");
            final byte[] screenshot = ((TakesScreenshot) scenarioContainer.webDriver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        }

        Thread.sleep(5000);
       // scenarioContainer.webDriver.quit();
    }
}
