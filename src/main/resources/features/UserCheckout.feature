Feature: Place an order on amazon website without login

  @sanity @critical
  Scenario: User search an item on Amazon website and add the item to the basket without login

    Given User is on Amazon website
    And User enter search item text as 'Java for Dummies by Barry A. Burd'
    And User clicks search button
    And User clicks on the first item from the list displayed on the website
    And User clicks on paperback option
    And User clicks AddToBasket button
    And User could see 1 item added to the basket
    When User clicks ProceedToCheckout button
    Then User is redirected to login page
